# Copyright (C) 2019, Fuzhou Rockchip Electronics Co., Ltd
# Released under the MIT license (see COPYING.MIT for the terms)

PATCHPATH = "${CURDIR}/u-boot"
inherit auto-patch

PV = "2017.09+git${SRCPV}"

LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"

inherit freeze-rev


# rk3399
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'rk3399', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=uboot/rk3399/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'rk3399', \
		   'c83e7a17b25c05a3005e31c2f51bc7769536a9d1', \
		   '', \
		   d)}"

# rk3328
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'rk3328', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=uboot/rk3399/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'rk3328', \
		   'c83e7a17b25c05a3005e31c2f51bc7769536a9d1', \
		   '', \
		   d)}"

# rk3288
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'rk3288', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=uboot/rk3288/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'rk3288', \
		   '01c726e03f1af51a270402eda3a272baf673bb6e', \
		   '', \
		   d)}"

# px30
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'px30', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=uboot/px30/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'px30', \
		   '0e41968cb744a84cf7fc7a1e13530af3febdc638', \
		   '', \
		   d)}"
