# Copyright (C) 2019, Fuzhou Rockchip Electronics Co., Ltd
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-kernel/linux/linux-yocto.inc
require linux-rockchip.inc

inherit freeze-rev

# rk3399
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'rk3399', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=kernel/rk3399/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'rk3399', \
		   '44a8d8fabe4b94d62ed24e19b7946084a0a5fe29', \
		   '', \
		   d)}"

# rk3328
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'rk3328', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=kernel/rk3399/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'rk3328', \
		   '44a8d8fabe4b94d62ed24e19b7946084a0a5fe29', \
		   '', \
		   d)}"

# rk3288
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'rk3288', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=kernel/rk3288/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'rk3288', \
		   '213586302617f113676d3ebb060b15ef6e11a47e', \
		   '', \
		   d)}"

# px30
SRC_URI = "${@bb.utils.contains('SOC_FAMILY', 'px30', \
		   ' git://github.com/T-Firefly/linux-mirrors.git;branch=kernel/px30/firefly;', \
		   '', \
		   d)}"

SRCREV = "${@bb.utils.contains('SOC_FAMILY', 'px30', \
		   'b825d028e1161e2403923bd19dddf512b3d326ac', \
		   '', \
		   d)}"

SRC_URI += "file://cgroups.cfg"

KERNEL_VERSION_SANITY_SKIP="1"
LINUX_VERSION = "4.4"

SRC_URI_append += "${@bb.utils.contains('IMAGE_FSTYPES', 'ext4', \
		   ' file://ext4.cfg', \
		   '', \
		   d)}"
